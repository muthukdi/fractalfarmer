import java.util.Random;

public class ArraySizeTest {
  
  static Random random = new Random(1);
  
  
  public static void main(String[] args) { 
    double a = 1.231234441;
    double b = 0.23449912;
    double c = 0.01112;
    
    int counter = 0;
    for(double i = b; i < a; i += c) {
      counter++;
    }
    
    System.out.println((a - b) / c); //> 89.63447131294964
    System.out.println(counter);  //> 90 
    
    
    
    float step = 1f;//random.nextFloat() * 0.1f;
    RectFloat rect1 = new RectFloat();
    int stepCounter = 0;
    System.out.println(rect1);
    System.out.println("Rect1 Width: " + rect1.getWidth());
    System.out.println("Rect1 Height: " + rect1.getHeight());
    for(float x = 0; x < rect1.getWidth(); x += step) {
      for(float y = 0; y < rect1.getHeight(); y += step) {
        stepCounter++;
      }
    }
    System.out.println(stepCounter);
    
    stepCounter = 0;
    for(float x = rect1.left; x < rect1.right; x += step) {
      for(float y = rect1.top; y > rect1.bottom; y -= step) {
        stepCounter++;
      }
    }
    System.out.println(stepCounter);
    
    //Testing the makeColumn funcitonality
    RectFloat[] rects = rect1.makeColumns(5, step);
    for(int i = 0; i < rects.length; i++) {
      System.out.println(rects[i]);
    }
    /*
     [RectFloat: top: 73.20096, left: -74.46627, bottom: -20.497602, right: -45.46627, width: 29.0, height: 93.69856] 
     [RectFloat: top: 73.20096, left: -45.46627, bottom: -20.497602, right: -16.46627, width: 29.0, height: 93.69856] 
     [RectFloat: top: 73.20096, left: -16.46627, bottom: -20.497602, right: 12.53373, width: 29.0, height: 93.69856] 
     [RectFloat: top: 73.20096, left: 12.53373, bottom: -20.497602, right: 41.53373, width: 29.0, height: 93.69856] 
     [RectFloat: top: 73.20096, left: 41.53373, bottom: -20.497602, right: 72.58033, width: 31.0466, height: 93.69856]  
     */
    
    
    
  }
  
  static class RectFloat {
    
    Random random;
    
    {
      random = new Random(100); 
    }
    
    public float top;
    public float left;
    public float bottom;
    public float right;
    
    RectFloat() {
      top = random.nextFloat() * 100 + 1;
      left = (-1) * random.nextFloat() * 100 - 1;
      
      bottom = (-1) * random.nextFloat() * 100 - 1;
      right = random.nextFloat() * 100 + 1;
    }
    
    RectFloat(float top, float left, float bottom, float right) {
      this.top = top;
      this.left = left;
      this.bottom = bottom;
      this.right = right;
    }
    
    float getWidth() {
      return this.right - this.left;
    }
    
    float getHeight() {
      return this.top - this.bottom;
    }
    
    public RectFloat[] makeColumns(int numOfColumns, float step) {
      //Find out how many steps we are going to make.
      //Is there a better, just-as-reliable, way?
      int stepCounter = 0;
      for(float x = this.left; x < this.right; x += step) {
        stepCounter++;
      }
      
      //Calculate the width in terms of elements in the output array
      int columnWidth = stepCounter / numOfColumns;
      
      //Create the output array
      RectFloat[] rects = new RectFloat[numOfColumns];
      
      //Build each column rect
      for(int i = 0; i < numOfColumns; i++) {
        float left = this.left + (i * columnWidth * step);
        float right = left + (columnWidth * step);
        rects[i] = new RectFloat(this.top, left, this.bottom, right);
      }
      
      //The final rect's right value will be off, because we have been multiplying by the rounded-down columnWidth value.
      //We fix it by just setting it to match the source rect's right value
      rects[rects.length - 1].right = this.right; 
      
      return rects;
    }
    
    public String toString() {
      return "[RectFloat: top: " + this.top + ", left: " + this.left + ", bottom: " + this.bottom + ", right: " + this.right+ ", width: " + this.getWidth() + ", height: " + this.getHeight() + "]"; 
    }
    
  }
  
}
