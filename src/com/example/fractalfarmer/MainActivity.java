package com.example.fractalfarmer;

import java.math.BigDecimal;

import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		//Must initialize Utils with a Context
		Utils.init(this);
		
		setContentView(R.layout.activity_main);
		//testBigDecimal();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		//Toast.makeText(this, item.getItemId(), Toast.LENGTH_LONG).show();
	    switch (item.getItemId())
	    {
	        case R.id.action_settings:
	        {
	    		Toast.makeText(this, "Settings", Toast.LENGTH_LONG).show();
	            startActivity(new Intent(this, PreferenceWithHeaders.class));
	            return true;
	        }
	    }
	    return false;
	}
	
	public void testBigDecimal()
	{
		MandelbrotCalculatorBD calculator = new MandelbrotCalculatorBD();
		BigDecimalPoint z1 = new BigDecimalPoint(new BigDecimal("1"), new BigDecimal("1"));
		BigDecimalPoint z2 = new BigDecimalPoint(new BigDecimal("2"), new BigDecimal("2"));
		BigDecimalPoint sum = calculator.getSum(z1, z2);
		Toast.makeText(this, "(" + sum.getX() + ", " + sum.getY() + ")", Toast.LENGTH_SHORT).show();
	}

}
