package com.example.fractalfarmer;

import java.util.HashMap;

/*
 H O W   T O   U S E


		int bench = Benchmarker.startBenchmark();			//Start the benchmark, save the int returned
		canvas.drawBitmap(bitmap, null, destRect, paint);	//Do the thing(s) you want to benchmark
		Benchmarker.endBenchmark(bench, "Drawing bitmap");	//End the benchmark and pass the int from the start
												
					//Pass a name too, and a println will be displayed with the name and milliseconds


 */

public class Benchmarker {

	private static HashMap<Integer, Long> startTimes;
	private static HashMap<Integer, Long> lapTimes;

	private Benchmarker(){}

	static {
		startTimes = new HashMap<Integer, Long>();
		lapTimes = new HashMap<Integer, Long>();
	}

	private static int counter;

	public synchronized static int startBenchmark() {
		Integer i = counter++;
		Long startTime = System.nanoTime();
		startTimes.put(i, startTime);

		return i;
	}

	public synchronized static void endBenchmark(int i, String name) {
		long endTime = System.nanoTime();
		long startTime = startTimes.get(i);
		startTimes.remove(i);
		float duration = (endTime - startTime) * 1.0f / 1000 / 1000;
		System.out.println(name + ": " + duration + " ms");
	}

	//	public static void lap(int i, String name) {
	//		long lapTime = System.nanoTime();
	//		Long l = lapTimes.get(i);
	//		long startTime;
	//		if(l == null) {
	//			startTime = startTimes.get(i);
	//			System.out.println(name + ": " + duration + " ms");
	//		} else {
	//			startTime = startTimes.get(i);
	//			float duration = (lapTime - startTime) * 1.0f / 1000 / 1000;
	//		
	//		}
	//		
	//	}



}
