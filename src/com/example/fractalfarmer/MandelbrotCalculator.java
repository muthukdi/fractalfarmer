package com.example.fractalfarmer;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

/*

		double a = 1.231234441;
		double b = 0.23449912;
		double c = 0.01112;
 
		int counter = 0;
		for(double i = b; i < a; i += c) {
			counter++;
		}
		
		System.out.println((a - b) / c);	//> 89.63447131294964
		System.out.println(counter);		//> 90 

*/
public class MandelbrotCalculator
{
	public final static short iterations = 250;
	
	public short[][] calculateRegion(Rect bitmapRegion, PointF origin, float step)
	{
		int arrayWidth = bitmapRegion.width();
		int arrayHeight = bitmapRegion.height();
		System.out.println("arrayWidth = " + arrayWidth + ", arrayHeight = " + arrayHeight);
		short[][] results = new short[arrayWidth][arrayHeight];
		int m = 0, n = 0;
		for(int i = bitmapRegion.left; i < bitmapRegion.right; i++)
		{
			float x = origin.x + step * i; // convert pixel to real number
			for(int j = bitmapRegion.top; j < bitmapRegion.bottom; j++)
			{
				float y = origin.y - step * j; // convert pixel to real number
				results[m][n] = checkValue(new PointF(x, y));
				n++;
			}
			m++;
			n = 0;
		}
		return results;
	}
	
	public short checkValue(PointF c)
	{
        PointF z = new PointF(0.0f, 0.0f);
        for (short i = 0; i < iterations; i++)
        {
            z = getSum(getSquare(z), c);
            if (getSquareModulus(z) > 4)
            {
                return i;
            }
        }
        return iterations;
	}
	
	public PointF getSquare(PointF z)
	{
		// Returns the square of the given complex number.
        PointF square = new PointF();
        square.set(z.x*z.x-z.y*z.y, 2*z.x*z.y);
        return square;
	}
	
	public float getSquareModulus(PointF z)
	{
		// Returns the square of the modulus 
		// of the given complex number.
        float reZ = z.x;
        float imZ = z.y;
        float modulus = reZ*reZ + imZ*imZ;
        return modulus;
	}
	
	public PointF getSum(PointF z1, PointF z2)
	{
		// Returns the sum of the two given complex numbers.
        PointF sum = new PointF();
        sum.set(z1.x + z2.x, z1.y + z2.y);
        return sum;
	}
}
