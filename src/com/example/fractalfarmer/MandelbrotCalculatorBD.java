package com.example.fractalfarmer;

import java.math.BigDecimal;

public class MandelbrotCalculatorBD
{
	private int iterations;
	
	public MandelbrotCalculatorBD()
	{
		iterations = 250;
	}
	
	public byte[][] calculateRegion(BigDecimalRect region, BigDecimal step)
	{
		return new byte[100][100];
	}
	
	public byte checkValue(BigDecimalPoint point)
	{
		return 0;
	}
	
	public BigDecimalPoint getSquare(BigDecimalPoint z)
	{
		return new BigDecimalPoint();
	}
	
	public BigDecimal getModulus(BigDecimalPoint z)
	{
		return new BigDecimal("0");
	}
	
	public BigDecimalPoint getSum(BigDecimalPoint z1, BigDecimalPoint z2)
	{
		BigDecimalPoint sum = new BigDecimalPoint(z1.getX().add(z2.getX()), z1.getY().add(z2.getY()));
		return sum;
	}
}
