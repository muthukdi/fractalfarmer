package com.example.fractalfarmer;

public class BigDecimalRect
{
	private BigDecimalPoint topLeft, bottomRight;
	
	public BigDecimalRect()
	{
		topLeft = new BigDecimalPoint();
		bottomRight = new BigDecimalPoint();
	}
	
	public BigDecimalRect(BigDecimalPoint topLeft, BigDecimalPoint bottomRight)
	{
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}
	
	public BigDecimalPoint getTopLeft()
	{
		return topLeft;
	}
	
	public BigDecimalPoint getBottomRight()
	{
		return bottomRight;
	}	
}
