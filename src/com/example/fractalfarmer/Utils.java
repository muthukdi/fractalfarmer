package com.example.fractalfarmer;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

public class Utils {
	
	static Context context;
	
	public static void init(Context c) {
		context = c;
	}
	
	public static String getLocalIpAddress() {
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
		
		return ip;
	}

}
