package com.example.fractalfarmer;

import java.math.BigDecimal;

public class BigDecimalPoint
{
	private BigDecimal x, y;
	
	public BigDecimalPoint()
	{
		x = new BigDecimal("0");
		y = new BigDecimal("0");
	}
	
	public BigDecimalPoint(BigDecimal x, BigDecimal y)
	{
		this.x = new BigDecimal(x.toString());
		this.y = new BigDecimal(y.toString());
	}
	
	public BigDecimal getX()
	{
		return x;
	}
	
	public BigDecimal getY()
	{
		return y;
	}
}
