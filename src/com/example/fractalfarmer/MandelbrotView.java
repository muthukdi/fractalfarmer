package com.example.fractalfarmer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.content.Context;

public class MandelbrotView extends View
{
	// View variables
	private Paint paint;
	private Bitmap bitmap;
	private int viewWidth, viewHeight;
	private Rect destRect;
	private int resolution; // max = 1
	Handler handler;
	
	// T : Screen -> C-plane
	private PointF origin; // T(0,0)
	private float step;
	
	public MandelbrotView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		paint = new Paint();
		resolution = 3;
		origin = new PointF(-2.0f, 1.0f);
		step = resolution*0.00226f;  // 0.00226 = region.height() / viewHeight
		handler = new Handler()
		{
			public void handleMessage(Message msg)
			{
				Object[] data = (Object[])msg.obj;
				short[][] results = (short[][])data[0];
				Rect bitmapRegion = (Rect)data[1];
				generateBitmapSection(bitmapRegion, results);
			}
		};
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		viewWidth = w;  // 1794
		viewHeight = h;  // 885
		bitmap = Bitmap.createBitmap(viewWidth/resolution, viewHeight/resolution, Bitmap.Config.ARGB_8888);
		System.out.println("Bitmap width = " + bitmap.getWidth() + ", height = " + bitmap.getHeight());
		destRect = new Rect(0, 0, viewWidth, viewHeight);
		// Divide the bitmap into five regions and then process
		// each of them independently on a separate thread.
		int regionWidth = bitmap.getWidth() / 5;
		int left = 0, top = 0, right = regionWidth, bottom = bitmap.getHeight();
		Rect bitmapRegion = new Rect(left, top, right, bottom);
		for (int i = 0; i < 4; i++)
		{
			new Thread(new MandelbrotThread(bitmapRegion)).start();
			left += regionWidth;
			right += regionWidth;
			bitmapRegion = new Rect(left, top, right, bottom);
		}
		//left += regionWidth;
		right = bitmap.getWidth();
		bitmapRegion = new Rect(left, top, right, bottom);
		new Thread(new MandelbrotThread(bitmapRegion)).start();
	}
	
	private void generateBitmapSection(Rect bitmapRegion, short[][] results)
	{
		int pixelColor;
		int m = 0, n = 0;
		for (int column = bitmapRegion.left; column < bitmapRegion.right; column++)
		{
			for (int row = bitmapRegion.top; row < bitmapRegion.bottom; row++)
			{
				pixelColor = Color.WHITE;
				short result = results[m][n];
				if (result == MandelbrotCalculator.iterations)
				{
					bitmap.setPixel(column, row, Color.BLACK);
				}
				else
				{
					for (int k = 0; k < 20; k++)
	                {
	                    if (result >= 0 && result < Math.pow(2,(float)k/2+1))
	                    {
	                        pixelColor = Color.rgb(255, 14*k, 14*k);
	                        break;
	                    }
	                }
					bitmap.setPixel(column, row, pixelColor);
				}
				n++;
			}
			m++;
			n = 0;
		}
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawBitmap(bitmap, null, destRect, paint);
	}
	
	private class MandelbrotThread implements Runnable
	{
		Rect bitmapRegion;
		public MandelbrotThread(Rect bitmapRegion)
		{
			this.bitmapRegion = bitmapRegion;
		}
		public void run()
		{
			MandelbrotCalculator mc = new MandelbrotCalculator();
			//int bench = Benchmarker.startBenchmark();
			short[][] results = mc.calculateRegion(bitmapRegion, origin, step);
			//Benchmarker.endBenchmark(bench, "Calcuating a region");
			Object[] data = {results, bitmapRegion};
			Message msg = Message.obtain();
			msg.obj = data;
			handler.sendMessage(msg);
		}
	}
}